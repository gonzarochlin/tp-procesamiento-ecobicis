import pandas as pd
import re
import matplotlib.pyplot as plt
# plt.style.use('ggplot')
# ver de cambiar el style
# plt.style.use('fivethirtyeight')

 
#2,2,2017-01-01 00:07:13,M,01/01/2017 00:07:13,11, Tribunales,84, Lavalle,0h 25min 8seg




def agrupador(valor):
    arra = valor.split(' ')

   #$ print(arra)
   # print (valor)
   # print(arra[0])
   # raise SystemExit

    minutos = int(re.sub('[^0-9]','', arra[1])) + int(re.sub('[^0-9]','', arra[0]))*60


    #Borrar ocn photoshop y poner otro nombre
    if minutos <= 20:
        return '<= 20 minutos'
    if minutos <= 40:
        return '> 20 minutos & <= 40 minutos'
    if minutos <= 60:
        return '> 40 minutos & <= 1 hora'
    if minutos <= 90:
        return '> 1 hora & <= 1 hora y media'
    if minutos <= 120:
        return '> 1 hora y media & <= 2 horas'   
    return '> 2 horas' 





dataFrameViajes = pd.read_csv('2017parsed.csv', header=0,sep = ',')
dataFrameViajes['date']=pd.to_datetime(dataFrameViajes['date'],dayfirst=1)
dataFrameViajes['tiempo_uso']=dataFrameViajes['tiempo_uso'].map(lambda a: agrupador(a))



##dataFrameViajes['tiempo_uso']=pd.to_datetime(dataFrameViajes['tiempo_uso'],format='%Hh %Mmin %Sseg',errors='coerce',dayfirst=1)


# cambiar a > 4 para tener el finde. Aca selecciono dias de semana
dataFrameViajesLV = dataFrameViajes[pd.to_datetime(dataFrameViajes['date']).dt.dayofweek  <= 4]
dataFrameViajesSD = dataFrameViajes[pd.to_datetime(dataFrameViajes['date']).dt.dayofweek  > 4]




dataFrameSumatoriaViajesLV=dataFrameViajesLV.groupby(by=dataFrameViajesLV['tiempo_uso'])['tiempo_uso'].count()
dataFrameSumatoriaViajesSD=dataFrameViajesSD.groupby(by=dataFrameViajesSD['tiempo_uso'])['tiempo_uso'].count()

#explsion
explode = (0.05,0.05,0.05,0.05,0.05,0.05)

# dataFrameSumatoriaViajesLV.plot.pie( title="Duracion del viaje dias de semana", figsize=(6, 6), y='', labels=['','','','','',''], autopct='%1.1f%%', explode=explode, startangle=90, pctdistance=0.85)
dataFrameSumatoriaViajesSD.plot.pie( title="Duracion del viaje fines de semana", figsize=(6, 6), y='', labels=['','','','','',''], autopct='%1.1f%%', explode=explode, startangle=90, pctdistance=0.85)

# plt.legend(dataFrameSumatoriaViajesLV.index, loc="best")
plt.legend(dataFrameSumatoriaViajesSD.index, loc="best")

#draw circle
centre_circle = plt.Circle((0,0),0.70,fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)

# Set aspect ratio to be equal so that pie is drawn as a circle.
plt.axis('equal')
plt.tight_layout()


plt.show()
#print(dataFrameSumatoriaViajes)

#dataFrameSumatoriaViajes.index.name = None



